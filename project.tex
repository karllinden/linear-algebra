\documentclass[a4paper]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{float}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage[hidelinks]{hyperref}
\usepackage{tabu}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\mathtoolsset{showonlyrefs}

\begin{document}

  \title{System of linear equations \\ problems D1-D8, p. 62}
  \author{
    Henrik \textsc{Ekstr\"{o}m} \\
    <henrik\_ekstrom\_42@hotmail.com> \\
    \and
    Karl \textsc{Lind\'{e}n} \\
    <karl.linden.887@student.lu.se> \\
    \and
    Khaled \textsc{Maana} \\
    <khaled.maana@gmail.com> \\
    \and
    Davide \textsc{Martino} \\
    <davide.martino.938@student.lu.se>
  }
  \maketitle

  \begin{center}
    \vfill
    {\small \includegraphics[width=2cm]{ccby}

    Copyright {\textcopyright} 2015 Henrik Ekstr\"{o}m, Karl Lind\'{e}n,
    Kaled Maana \& Davide Martino.
    This work is licensed under the Creative Commons Attribution 4.0
    International License. To view a copy of this license, visit
    \url{http://creativecommons.org/licenses/by/4.0/}.}

    \vspace{0.5cm}

    The entire project can be downloaded from
    \url{https://gitlab.com/lilrc/linear-algebra}.
  \end{center}
  \clearpage

  \begin{enumerate}[label=\bf{D\arabic*}]
    \item
    If the homogeneous linear system has only the trivial solution, it
    means there are no free variables in the linear system.
    Thus, if we put the linear system of equations into reduced row
    echelon form we must get a leading 1 in each row.
    The same holds for the nonhomogeneous system, which means its
    solution is unique and non-trivial.

    \item
    \begin{enumerate}[label=\bf(\alph*)]
      \item
      All lines intersect the origin.
      Since the system only has the trivial solution then at least one
      line is not parallel to all of the others.

      \item
      That the system has infinitely many solutions means that the
      lines are equal.
    \end{enumerate}

    \item
    \begin{enumerate}[label=\bf(\alph*)]
      \item
      If \((x,y) = (x_0, y_0)\) is a solution of the system and \(k\) is
      any constant, then \((x,y) = (kx_0, ky_0)\) is also a solution of
      the system.

      According to the assumptions:
      \begin{equation}
        \begin{cases}
          ax_0 + by_0 = 0 \\
          cx_0 + dy_0 = 0
        \end{cases}
      \end{equation}

      Multiply each equation by the constant \(k \neq 0\), then

      \begin{equation}
        \begin{split}
          \begin{cases}
            k(ax_0 + by_0) = 0 \\
            k(cx_0 + dy_0) = 0
          \end{cases}
          &\iff
          \begin{cases}
            kax_0 + kby_0 = 0 \\
            kcx_0 + kdy_0 = 0
          \end{cases} \\
          &\iff
          \begin{cases}
            a(kx_0) + b(ky_0) = 0 \\
            c(kx_0) + d(ky_0) = 0
          \end{cases}
        \end{split}
      \end{equation}
      which proves \((x,y) = (kx_0, ky_0)\) is also a solution of the
      system.

      Now assume \(k = 0\), the ordered pair \((x,y) = (0, 0)\) makes
      every equation in the system a true statement, therefore it is a
      solution of the system.

      \item
      If \((x,y) = (x_0, y_0)\) and \((x,y) = (x_1, y_1)\) are solution
      of the system, then \((x, y) = (x_0 + x_1, y_0 + y_1)\) is also a
      solution of the system.

      According to the assumptions we have
      \begin{equation}
        \begin{cases}
            ax_0 + by_0 = 0 \\
            cx_0 + dy_0 = 0
        \end{cases}
        \quad
        \text{and}
        \quad
        \begin{cases}
            ax_1 + by_1 = 0 \\
            cx_1 + dy_1 = 0
        \end{cases}
      \end{equation}

      \[ \implies \]

      \begin{equation}
        \begin{cases}
          (ax_0 + by_0) + (ax_1 + by_1) = 0 \\
          (cx_0 + dy_0) + (cx_1 + dy_1) = 0
        \end{cases}
      \end{equation}

      \[ \iff \]

      \begin{equation}
        \begin{cases}
          a(x_0 + x_1) + b(y_0 + y_1) = 0 \\
          c(x_0 + x_1) + d(y_0 + y_1) = 0
        \end{cases}
      \end{equation}
      which proves that \((x,y) = (x_0 + x_1, y_0 + y_1)\) is also a
      solution of the system.

      \item
      If the pair \((x,y) = (x_0, y_0)\) solves the non-homogeneous
      linear system
      \begin{equation}
        \label{eq:non-hom}
        \begin{cases}
          ax_0 + by_0 = e \\
          cx_0 + dy_0 = f
        \end{cases}
      \end{equation}
      then \((x,y) = (kx_0, ky_0)\) is not a solution of the same
      system.

      Assume \(k \neq 0\), then
      \begin{equation}
        \begin{cases}
          k(ax_0 + by_0) = ke \\
          k(cx_0 + dy_0) = kf
        \end{cases}
        \iff
        \begin{cases}
          a(kx_0) + b(ky_0) = ke \\
          c(kx_0) + d(ky_0) = kf
        \end{cases}
      \end{equation}
      which shows that the point \((x,y) = (kx_0, ky_0)\) is a solution
      of \eqref{eq:non-hom} if and only if k = 1.

      If \((x,y) = (x_0, y_0)\) and \((x,y) = (x_1, y_1)\) are solutions
      of \eqref{eq:non-hom}, then \((x,y) = (x_0+x_1,y_0+y_1)\) is in
      general not a solution to \eqref{eq:non-hom}.
      We have

      \begin{equation}
        \begin{cases}
          ax_0 + by_0 = e \\
          cx_0 + dy_0 = f
        \end{cases}
        \quad
        \text{and}
        \quad
        \begin{cases}
          ax_1 + by_1 = e \\
          cx_1 + dy_1 = f
        \end{cases}
      \end{equation}

      \[ \implies \]

      \begin{equation}
        \begin{cases}
          (ax_0 + by_0) + (ax_1 + by_1) = 2e \\
          (cx_0 + dy_0) + (cx_1 + dy_1) = 2f
        \end{cases}
      \end{equation}

      \[ \iff \]

      \begin{equation}
        \begin{cases}
          a(x_0 + x_1) + b(y_0 + y_1) = 2e \\
          c(x_0 + x_1) + d(y_0 + y_1) = 2f
        \end{cases}
      \end{equation}
      which shows that the sum \((x,y) = (x_0+x_1,y_0+y_1)\) solves a
      different system.
    \end{enumerate}

    \item
    The following linear systems are given.
    \begin{equation}
      \label{eq:D4-non-hom}
      \begin{cases}
        ax + by = k \\
        cx + dy = l
      \end{cases}
    \end{equation}

    \begin{equation}
      \label{eq:D4-hom}
      \begin{cases}
        ax + by = 0 \\
        cx + dy = 0
      \end{cases}
    \end{equation}

    The solution set of the non-homogeneous linear system
    \eqref{eq:D4-non-hom}, if not empty, is composed by translating the
    solution set of the corresponding homogeneous linear system
    \eqref{eq:D4-hom} by one particular solution of
    \eqref{eq:D4-non-hom}.

    % D5
    \item
    \begin{enumerate}[label=\bf(\alph*)]
      \item
      If \(A\) is a \(3 \times 5\) matrix, then the number of leading
      ones in its reduced row echelon form is at most \textbf{three}.

      \item
      If \(B\) is a \(3 \times 6\) matrix whose last column has all
      zeros, then the number of parameters in the general solution of
      the linear system with augmented matrix \(B\) is at most
      \textbf{four}.
      If \(B\) has three leading ones, the maximum is two.

      \item
      If \(A\) is a \(3 \times 5\) matrix, then the number of leading
      ones in any of its row echelon forms is at most \textbf{three}.
    \end{enumerate}
    In both (a) and (c) there can not be more leading ones than number
    of rows.

    The linear system in (b) is homogeneous and has three equations with
    five unknowns.
    At least one element of \(B\) must be nonzero, or else it is not a
    linear system of equations, but it does not need to have more
    nonzero elements.
    Thus at least one variable is bound, which leaves at most four free
    variables.

    % D6
    \item
    \begin{enumerate}[label=\bf(\alph*)]
      \item
      If \(A\) is a \(5 \times 3\) matrix, then the number of leading
      ones in its reduced row echelon form is at most \textbf{three}.

      \item
      If \(B\) is a \(5 \times 4\) matrix whose last column has all
      zeros, then the number of parameters in the general solution of
      the linear system with augmented matrix \(B\) is at most
      \textbf{two}.
      If \(B\) has three leading ones, the maximum is two.

      \item
      If \(A\) is a \(5 \times 3\) matrix, then the number of leading
      ones in any of its row echelon forms is at most \textbf{three}.
    \end{enumerate}
    In both (a) and (c) there can not be more leading ones than number
    of columns.

    The linear system in (b) is homogeneous and has five equations with
    three unknowns.
    At least one element of \(B\) must be nonzero, or else it is not a
    linear system of equations, but it does not need to have more
    nonzero elements. Thus at least one variable is bound, which leaves
    at most two free variables.

    \item
    \begin{enumerate}[label=\bf{(\alph*)}]
      \item
      The statement is \textbf{false}.

      The linear system
      \[
        \begin{cases}
          x + y + z = 0 \\
          x + y + z = 1
        \end{cases}
      \]
      has more unknowns than equations, but is inconsistent.

      \item
      The statement is \textbf{false}.

      If the system is homogeneous it has either only the trivial
      solution or infinitely many solutions (theorem 2.2.1).
      It cannot have exactly two solutions.

      Assume a nonhomogeneous system has two unique solutions
      \(\bf{x_1}\) and \(\bf{x_2}\).
      If the corresponding homogeneous system has infinitely many
      solutions \(\bf{x_h}\) then \(\bf{x_1} + \bf{x_h}\) must be a
      solution to the nonhomegenous system, which shows that the
      nonhomogeneous system has infinitely many solutions.
      If the corresponding homogeneous system has only the trivial
      solution, \(\bf{x_h} = 0\), then, since \(\bf{x_1} - \bf{x_2}\) is
      a solution to the homogeneous system,
      \(\bf{x_1} - \bf{x_2} = 0\), which means that
      \(\bf{x_1} = \bf{x_2}\).
      Thus the solution to the nonhomogeneous system is unique.
      In none of the cases there are exactly two solutions.

      \item
      The statement is \textbf{false}.

      There are infinitely many solutions to the homogeneous system
      (theorem 2.2.3).
      If the system is nonhomogeneous then the homogeneous solution can
      be added to a partial solution to the nonhomogeneous system, which
      yields infinitely many solutions.

      \item
      The statement is \textbf{true}.

      A homogeneous system always has the trivial solution
      \(\bf x = \bf 0\).
    \end{enumerate}

    \item
    \begin{enumerate}[label=\bf{(\alph*)}]
      \item
      The statement is \textbf{true}.

      The matrix
      \[
        \begin{pmatrix}
          1 & -1 & 0 \\
          1 & 0 & 0
        \end{pmatrix}
      \]
      has (atleast) the two row echelon forms below.
      \[
        \begin{pmatrix}
          1 & -1 & 0 \\
          0 & 1 & 0
        \end{pmatrix}
        \text{ and }
        \begin{pmatrix}
          1 & 0 & 0 \\
          0 & 1 & 0
        \end{pmatrix}
      \]

      \item
      The statement is \textbf{false}.

      It can be proven that the reduced row echelon form is unique. A
      proof can be found in the article ``The Reduced Row Echelon Form
      of a Matrix Is Unique: A Simple Proof'' by Thomas Yuster,
      \textit{Mathematics Magazine}, Vol. 57, No. 2, 1984, pp. 93-94.

      \item
      The statement is \textbf{false}.

      The system
      \[
        \begin{cases}
          x + y = 0 \\
          x + y = 1 \\
          x + y = 0
        \end{cases}
      \]
      has the augmented matrix
      \[
        \begin{pmatrix}
          1 & 1 & 0 \\
          1 & 1 & 1 \\
          1 & 1 & 0
        \end{pmatrix},
      \]
      which has the reduced row echelon form
      \[
        \begin{pmatrix}
          1 & 1 & 0 \\
          0 & 0 & 1 \\
          0 & 0 & 0
        \end{pmatrix}.
      \]
      Clearly the row echelon form has a row consisting entirely of
      zeroes, but the system lacks solution, which shows the assertion.

      \item
      The statement is \textbf{false}.

      The nonhomogeneous system
      \[
        \begin{cases}
          x + y = 2 \\
          x - y = 0 \\
          x + y = 2
        \end{cases}
      \]
      has more equations than unknowns, but is consistent since it has
      the solution \((x,y) = (1,1)\).
    \end{enumerate}
  \end{enumerate}

\end{document}
